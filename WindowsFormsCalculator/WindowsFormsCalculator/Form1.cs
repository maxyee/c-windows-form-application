﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsCalculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Add(object sender, EventArgs e)
        {
            int input1 = Convert.ToInt32(txtInput1.Text);

            int input2 = Convert.ToInt32(txtInput2.Text);

            int result = input1 + input2;

            txtResult.Text = result.ToString();
        }
    }
}
