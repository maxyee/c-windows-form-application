﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsListbox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void AddFromCode(object sender, EventArgs e)
        {
            if(txtInput.Text != "")
            {
                lstItems.Items.Add(txtInput.Text);
            }
            else
            {
                MessageBox.Show("Please give some Text");
            }
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void AddItemFromArray(object sender, EventArgs e)
        {
            string[] data = {"Mohon","Sumon","Sujon"};
            lstItems.Items.AddRange(data);
        }

        private void getSelectedIndex(object sender, EventArgs e)
        {
            MessageBox.Show(lstItems.SelectedIndex.ToString());
        }

        private void getSelectedItem(object sender, EventArgs e)
        {
            MessageBox.Show(lstItems.SelectedItem.ToString());
        }

        private void btnclickmebutton(object sender, EventArgs e)
        {
            btnClickMe.Text = "Ohh!!You Cliked Me!!";
        }
    }
}
