﻿namespace WindowsFormsListbox
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstItems = new System.Windows.Forms.ListBox();
            this.btnAddFormCode = new System.Windows.Forms.Button();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.btnAddItemFromArray = new System.Windows.Forms.Button();
            this.GetSelectedIndex = new System.Windows.Forms.Button();
            this.GetSelectedItem = new System.Windows.Forms.Button();
            this.btnClickMe = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstItems
            // 
            this.lstItems.FormattingEnabled = true;
            this.lstItems.Items.AddRange(new object[] {
            "a",
            "b",
            "c"});
            this.lstItems.Location = new System.Drawing.Point(12, 13);
            this.lstItems.Name = "lstItems";
            this.lstItems.Size = new System.Drawing.Size(260, 95);
            this.lstItems.TabIndex = 0;
            // 
            // btnAddFormCode
            // 
            this.btnAddFormCode.Location = new System.Drawing.Point(12, 161);
            this.btnAddFormCode.Name = "btnAddFormCode";
            this.btnAddFormCode.Size = new System.Drawing.Size(260, 23);
            this.btnAddFormCode.TabIndex = 1;
            this.btnAddFormCode.Text = "Add An Item From Code";
            this.btnAddFormCode.UseVisualStyleBackColor = true;
            this.btnAddFormCode.Click += new System.EventHandler(this.AddFromCode);
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(12, 124);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(260, 20);
            this.txtInput.TabIndex = 2;
            this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
            // 
            // btnAddItemFromArray
            // 
            this.btnAddItemFromArray.Location = new System.Drawing.Point(13, 191);
            this.btnAddItemFromArray.Name = "btnAddItemFromArray";
            this.btnAddItemFromArray.Size = new System.Drawing.Size(259, 23);
            this.btnAddItemFromArray.TabIndex = 3;
            this.btnAddItemFromArray.Text = "Add Iteam From Array";
            this.btnAddItemFromArray.UseVisualStyleBackColor = true;
            this.btnAddItemFromArray.Click += new System.EventHandler(this.AddItemFromArray);
            // 
            // GetSelectedIndex
            // 
            this.GetSelectedIndex.Location = new System.Drawing.Point(12, 221);
            this.GetSelectedIndex.Name = "GetSelectedIndex";
            this.GetSelectedIndex.Size = new System.Drawing.Size(260, 23);
            this.GetSelectedIndex.TabIndex = 4;
            this.GetSelectedIndex.Text = "Get Selected Index";
            this.GetSelectedIndex.UseVisualStyleBackColor = true;
            this.GetSelectedIndex.Click += new System.EventHandler(this.getSelectedIndex);
            // 
            // GetSelectedItem
            // 
            this.GetSelectedItem.Location = new System.Drawing.Point(13, 251);
            this.GetSelectedItem.Name = "GetSelectedItem";
            this.GetSelectedItem.Size = new System.Drawing.Size(259, 23);
            this.GetSelectedItem.TabIndex = 5;
            this.GetSelectedItem.Text = "Get Selected Item";
            this.GetSelectedItem.UseVisualStyleBackColor = true;
            this.GetSelectedItem.Click += new System.EventHandler(this.getSelectedItem);
            // 
            // btnClickMe
            // 
            this.btnClickMe.Location = new System.Drawing.Point(13, 281);
            this.btnClickMe.Name = "btnClickMe";
            this.btnClickMe.Size = new System.Drawing.Size(259, 23);
            this.btnClickMe.TabIndex = 6;
            this.btnClickMe.Text = "Click Me";
            this.btnClickMe.UseVisualStyleBackColor = true;
            this.btnClickMe.Click += new System.EventHandler(this.btnclickmebutton);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(296, 347);
            this.Controls.Add(this.btnClickMe);
            this.Controls.Add(this.GetSelectedItem);
            this.Controls.Add(this.GetSelectedIndex);
            this.Controls.Add(this.btnAddItemFromArray);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.btnAddFormCode);
            this.Controls.Add(this.lstItems);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstItems;
        private System.Windows.Forms.Button btnAddFormCode;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Button btnAddItemFromArray;
        private System.Windows.Forms.Button GetSelectedIndex;
        private System.Windows.Forms.Button GetSelectedItem;
        private System.Windows.Forms.Button btnClickMe;
    }
}

